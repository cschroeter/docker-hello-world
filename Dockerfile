# You should always specify a full version here to ensure all of your developers
# are running the same version of Node.
FROM node:9-slim

# Override the base log level (info).
ENV NPM_CONFIG_LOGLEVEL warn

# Install and configure `serve`.
RUN yarn global add serve
CMD serve -s dist
EXPOSE 5000

# Copy all local files into the image.
COPY . /usr/src/app

# Set Workdir
WORKDIR /usr/src/app

